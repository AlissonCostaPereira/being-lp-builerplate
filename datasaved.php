<?php 
session_start();

if (isset($_POST) && empty($_POST)) {
	exit();	
} else{

	require_once 'securit/security.csrf.php';
	require_once 'methods.php';
	require_once 'connect.class.php';

	global $security;
	$security = new \security\CSRF;
	
	if(isset($_POST['token'])) {
		
		if($security->get($_POST['token'])) {

			$conexao = new Connect();
			$conexao->getInstance();

			$objeto = new Methods();
			$objeto->AjaxSaveData();
			
		} else {

			exit();

		}
	}
}
?>