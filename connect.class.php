<?php

	require_once 'datasaved.php';
	require_once 'securit/security.csrf.php';
	global $security;
	
class connException extends Exception { }
class Connect{



	// Conexão
	const DB_HOST='localhost';
	const DB_NOME='teste_bd';
	const DB_USER='root';
	const DB_PASS='';
	const DB_PREFIX='';

	private static $instance = NULL;
	// global $security;

	function __construct(){

	}

	/**
	 * Método que retorna o prefixo das tabelas.
	 * Esse prefixo foi setado na instalação do sistema
	 */
	public static function getPrefix(){
		return self::DB_PREFIX;
	}

	/**
	 * Método que verifica se as constantes foram geradas ou não.
	 * @return Boolean - Caso as constantes tenham sido geradas na instalação, retorna TRUE;
	 */
	public static function checkConstants(){
		return (self::DB_HOST=='[BD_HOST]' ||  self::DB_NOME=='[BD_NOME]' ||	self::DB_USER=='[BD_USER]' ||	self::DB_PASS=='[BD_PASS]' || self::DB_PREFIX=='[BD_PREFIX]') ? false : true;
	}

	/**
	 * Método de conexão ao banco.
	 * Utiliza a extensão PDO, que serve para utilizar vários modelos de banco de dados.
	 */
	public static function getInstance()
	{
		$security = new \security\CSRF;
		try {
			if(self::checkConstants()){
				if (self::$instance===NULL){
					try{
						
						$security->delete($_POST['token']);

					   	self::$instance = @new PDO("mysql:host=".self::DB_HOST.";dbname=".self::DB_NOME, self::DB_USER, self::DB_PASS);
						return self::$instance;
					} catch(PDOException $e) {
					   die("N&atilde;o foi poss&iacute;vel conectar &agrave; fonte de dados. <br/> Detalhes: ". $e->getMessage());
					}
				}
				return self::$instance;
			}else{
				throw new connException('Contantes de acesso ao banco de dados não definidas!');
			};
		}catch (Exception $e) {
			throw $e;
		}
	}

	public static function validadeConnection($DB_HOST,$DB_NOME,$DB_USER,$DB_PASS){
		try{
		   self::$instance = @new PDO("mysql:host=".$DB_HOST.";dbname=".$DB_NOME, $DB_USER, $DB_PASS);
			return self::$instance;
		} catch(PDOException $e) {
			throw $e->getMessage();
		}
	}

} // End class Connect
