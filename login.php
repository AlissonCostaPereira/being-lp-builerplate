<?php include 'includes/header.php'; ?>
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="content">
				<div class="row">
					<form>
						<div class="col-sm-12">
							<div class="form-group">
								<label for="user">Usuário</label>
								<input type="text" class="form-control" id="user" placeholder="Digite seu Usuário">
							</div>
						</div>
						<div class="col-sm-12">
							<div class="form-group">
								<label for="pass">Senha</label>
								<input type="password" class="form-control" id="pass" placeholder="Digite sua senha">
							</div>
						</div>
						<div class="col-sm-12">
							<input type="button" name="enviar" value="Enviar" class="pull-right">
							<input type="hidden" name="token" value="<?php echo $token; ?>">
						</div>
					</form>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="g-recaptcha" data-sitekey="6Lc6h0sUAAAAAKNDpxyKyRpZCydfuVo1rSUBUK8V"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>