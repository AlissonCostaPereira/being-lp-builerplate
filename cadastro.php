<?php include 'includes/header.php'; ?>
<section class="Contato">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
				<h2>Contato</h2>
				<form name="form_inscricao" class="formulario needs-validation" action="datasaved.php" method="post" novalidate>
					<div class="form-row">
						
						<div class="col-sm-12">
							<input type="text" id="nome" name="nome" class="form-control" data-required="true" placeholder="Nome">
						</div>
						<div class="col-sm-6">
							<input type="text" id="email" name="email" class="form-control" data-required="true" placeholder="E-mail">
						</div>
						<div class="col-sm-6">
							<input type="text" id="telefone" name="telefone" class="form-control" data-required="true" placeholder="Telefone">
						</div>
						<div class="col-sm-6">
							<input type="text" id="usuario" name="usuario" class="form-control" data-required="true" placeholder="usuario">
						</div>
						<div class="col-sm-3">
							<input type="password" id="senha_1" name="senha_1" class="form-control" data-required="true" placeholder="senha">
						</div>
						<div class="col-sm-3">
							<input type="password" id="senha_2" name="senha_2" class="form-control" data-required="true" placeholder="senha">
						</div>
						<div class="col-sm-12">
							<textarea rows="6" class="form-control" data-required="true" id="mensagem" name="mensagem" placeholder="Mensagem"></textarea>
							<input type="hidden" name="token" value="<?php echo $token; ?>">
							<input type="button" name="enviar" value="enviar" onclick="javascript: return validar();">
						</div>
					</div>
					<div class="g-recaptcha" data-sitekey="6Lc6h0sUAAAAAKNDpxyKyRpZCydfuVo1rSUBUK8V"></div>
				</form>
			</div>
		</div>
	</div>
</section>
<?php include 'includes/footer.php'; ?>